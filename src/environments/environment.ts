// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDV1AFCso3vc6NyWCNskgzJ47UAXockGGY",
    authDomain: "parcial2-9b86e.firebaseapp.com",
    databaseURL: "https://parcial2-9b86e-default-rtdb.firebaseio.com",
    projectId: "parcial2-9b86e",
    storageBucket: "parcial2-9b86e.appspot.com",
    messagingSenderId: "1093586497467",
    appId: "1:1093586497467:web:5d1f0a2c02f8255883de9e",
    measurementId: "G-52ZTM3NGJ0"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
