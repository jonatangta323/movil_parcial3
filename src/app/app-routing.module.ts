import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'listar-riesgo',
    loadChildren: () => import('./pages/listar-riesgo/listar-riesgo.module').then( m => m.ListarRiesgoPageModule)
  },
  {
    path: 'registrar-paciente',
    loadChildren: () => import('./pages/registrar-paciente/registrar-paciente.module').then( m => m.RegistrarPacientePageModule)
  },
  {
    path: 'listar-inscritos',
    loadChildren: () => import('./pages/listar-inscritos/listar-inscritos.module').then( m => m.ListarInscritosPageModule)
  },
  {
    path: 'taller',
    loadChildren: () => import('./pages/taller/taller.module').then( m => m.TallerPageModule)
  },
  {
    path: 'modal-taller',
    loadChildren: () => import('./pages/modal-taller/modal-taller.module').then( m => m.ModalTallerPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'registro',
    loadChildren: () => import('./pages/registro/registro.module').then( m => m.RegistroPageModule)
  },
  {
    path: 'create-event',
    loadChildren: () => import('./pages/create-event/create-event.module').then( m => m.CreateEventPageModule)
  },
  {
    path: 'listo-evento',
    loadChildren: () => import('./pages/listo-evento/listo-evento.module').then( m => m.ListoEventoPageModule)
  },
  {
    path: 'detalles-evento/:id',
    loadChildren: () => import('./pages/detalles-evento/detalles-evento.module').then( m => m.DetallesEventoPageModule)
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
