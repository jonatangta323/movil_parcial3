import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';

@Injectable({
  providedIn: 'root'
})
export class TalleresService {

  constructor(
    private firestore: AngularFirestore
  ) {}
  listarTaller(){
    return this.firestore.collection('talleres').snapshotChanges()
  }
}
