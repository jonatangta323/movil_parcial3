import { Component, OnInit } from '@angular/core';
import { EventService } from 'src/app/servis/event.service';

@Component({
  selector: 'app-listo-evento',
  templateUrl: './listo-evento.page.html',
  styleUrls: ['./listo-evento.page.scss'],
})
export class ListoEventoPage implements OnInit {
  eventList:any=[];
  constructor(private es:EventService) { }

  ngOnInit() {
    this.es.getEventList().then(evenListSnapshot=>  { 
     this.eventList=[];
     evenListSnapshot.forEach(snap=>{ this.eventList.push({
      id: snap.id, 
      name: snap.data().name,
      price: snap.data().price,
      date: snap.data().date }); 

       return false;
          });
     });
    }

}
