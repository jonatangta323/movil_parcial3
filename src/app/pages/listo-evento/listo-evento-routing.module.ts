import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListoEventoPage } from './listo-evento.page';

const routes: Routes = [
  {
    path: '',
    component: ListoEventoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListoEventoPageRoutingModule {}
