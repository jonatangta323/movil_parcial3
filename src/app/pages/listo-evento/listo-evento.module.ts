import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListoEventoPageRoutingModule } from './listo-evento-routing.module';

import { ListoEventoPage } from './listo-evento.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListoEventoPageRoutingModule
  ],
  declarations: [ListoEventoPage]
})
export class ListoEventoPageModule {}
