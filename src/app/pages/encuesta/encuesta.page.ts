import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import {ActivatedRoute} from '@angular/router';
import { AlertController } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-encuesta',
  templateUrl: './encuesta.page.html',
  styleUrls: ['./encuesta.page.scss'],
})
export class EncuestaPage implements OnInit {

  opciones=[];
  resultado=0;
  nombre="";
  cedulas: Observable<any>
   constructor(private navCtrl:NavController,private ruta:ActivatedRoute,public alertCtrl: AlertController,
    firestore: AngularFirestore) {
      this.cedulas = firestore.collection('pacientes').valueChanges()
    }
 
   ngOnInit(){
   }
 
   GuardarOpciones(){
     console.log(this.nombre);
     if(this.nombre!="" && this.opciones[0]!=null && this.opciones[1]!=null && this.opciones[2]!=null
      && this.opciones[3]!=null && this.opciones[4]!=null && this.opciones[5]!=null && this.opciones[6]!=null
      && this.opciones[7]!=null && this.opciones[8]!=null && this.opciones[9]!=null && this.opciones[10]!=null
      && this.opciones[11]!=null){
     var i=0;
     for(i=0;i<=this.opciones.length-1;i++){
       this.resultado+=parseInt(this.opciones[i]);
     }
     console.log(this.opciones);
     console.log(this.resultado);
     this.navCtrl.navigateForward('/resultado/'+this.resultado+'/'+this.nombre);

    }else{
      this.presentAlert();
    }

   }

   async presentAlert() {
    const alert = await this.alertCtrl.create({
    message: 'Por Favor Ingrese todos los datos',
    subHeader: 'Advertencia',
    buttons: ['ACEPTAR']
   });
   await alert.present(); 
  }

}
