import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/servis/login.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  correo = ''
  pass = ''
  constructor(
    private auth: LoginService,
    private router: Router,
    public alertController: AlertController) { }

  ngOnInit() {
  }
  login() {
    if(this.correo!="" && this.pass!=""){
      this.auth.login(this.correo, this.pass).then(res => {
        if (res.user) {
          this.router.navigate(['home'])
        }else{
          console.log("Error de login")
          this.presentAlert();
        }
      })

    }else{
      this.presentAlert2();
    }
 
    
  }

  async presentAlert() {
    const alert = await this.alertController.create({
    message: 'Correo o Contraseña incorrectos',
    subHeader: 'Advertencia',
    buttons: ['ACEPTAR']
   });
   await alert.present(); 
  }

  async presentAlert2() {
    const alert = await this.alertController.create({
    message: 'Por favor ingrese su correo y su contraseña',
    subHeader: 'Advertencia',
    buttons: ['ACEPTAR']
   });
   await alert.present(); 
  }

}
