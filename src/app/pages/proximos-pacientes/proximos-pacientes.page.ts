import { Component, OnInit } from '@angular/core';
import { PacientesService } from 'src/app/pages/proximos-pacientes/proximos-pacientes.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-proximos-pacientes',
  templateUrl: './proximos-pacientes.page.html',
  styleUrls: ['./proximos-pacientes.page.scss'],
})
export class ProximosPacientesPage implements OnInit {

  users:Observable<any>;
  constructor(private pacientesService:PacientesService) {
    this.users=this.pacientesService.getPacientes();
    console.log(this.users);
  }

  ngOnInit() {
  
  }

}
