import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PacientesService {

  constructor(private http:HttpClient) { }

  getPacientes():Observable<any>{
   return this.http.get('https://jsonplaceholder.typicode.com/users')
  }


  

}
