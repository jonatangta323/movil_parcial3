import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProximosPacientesPageRoutingModule } from './proximos-pacientes-routing.module';

import { ProximosPacientesPage } from './proximos-pacientes.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProximosPacientesPageRoutingModule
  ],
  declarations: [ProximosPacientesPage]
})
export class ProximosPacientesPageModule {}
