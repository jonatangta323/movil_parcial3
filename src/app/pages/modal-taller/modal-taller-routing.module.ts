import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModalTallerPage } from './modal-taller.page';

const routes: Routes = [
  {
    path: '',
    component: ModalTallerPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModalTallerPageRoutingModule {}
