import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModalTallerPageRoutingModule } from './modal-taller-routing.module';

import { ModalTallerPage } from './modal-taller.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModalTallerPageRoutingModule
  ],
  declarations: [ModalTallerPage]
})
export class ModalTallerPageModule {}
