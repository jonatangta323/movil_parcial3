import { Component, OnInit, Input } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
@Component({
  selector: 'app-modal-taller',
  templateUrl: './modal-taller.page.html',
  styleUrls: ['./modal-taller.page.scss'],
})
export class ModalTallerPage implements OnInit {

  @Input() nombre: string;
  @Input() descripcion: string;
  @Input() precio: string;

  constructor( 
    public alertCtrl: AlertController,
    public modalController: ModalController) { }

  ngOnInit() {
  }

   async inscripcion(nombre){
    let alert = await this.alertCtrl.create({
      message: '¿Quiere inscribirse en este taller:'+ nombre+ '?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            this.confirmacion();
          }
        }
      ]
    });
    await alert.present();
  }

  cerrar(){
    this.modalController.dismiss({
      'dismissed': true
    });
  }

  async confirmacion(){
    const alert = await this.alertCtrl.create({
      message: 'Su inscripcion en el taller ha sido exitosa',
      subHeader: 'Confirmacion',
      buttons: ['ACEPTAR']
     });
     await alert.present(); 
     this.modalController.dismiss({
      'dismissed': true
    });

  }

}
