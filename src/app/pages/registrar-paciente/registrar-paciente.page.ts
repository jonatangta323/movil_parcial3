import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-registrar-paciente',
  templateUrl: './registrar-paciente.page.html',
  styleUrls: ['./registrar-paciente.page.scss'],
})
export class RegistrarPacientePage implements OnInit {
  cedula;
  nombre="";
  apellido="";
  edad;
  telefono;
  direccion="";
  civil="";
  profesion="";
  estrato="";
  cargo="";
  horas;
  cantidad;
  nivel="";

  constructor(
    public alertCtrl: AlertController,
    private firestore: AngularFirestore
    ) { }

  ngOnInit() {
  }

  registrar(){
    if(this.cedula!=null && this.nombre!="" && this.apellido!=""
    && this.edad!=null && this.telefono!=null && this.direccion!=""
    && this.civil!="" && this.profesion!="" && this.estrato!=""
    && this.cargo!="" && this.horas!=null && this.cantidad!=null
    && this.nivel!=""){
    const paciente = {
      nombres: this.nombre,
      cedula: this.cedula,
      apellidos: this.apellido,
      direccion: this.direccion,
      edad: this.edad,
      telefono: this.telefono,
      estado_civil: this.civil,
      profesion: this.profesion,
      estrato: this.estrato,
      cargo: this.cargo,
      horas: this.horas,
      cantidad_ali: this.cantidad,
      nivel_edu: this.nivel
    }
    this.firestore.collection('pacientes').add(paciente)
    alert("Agregado")
    }else{
      this.presentAlert();

    }

  }

  async presentAlert() {
    const alert = await this.alertCtrl.create({
    message: 'Por Favor Ingrese todos los datos',
    subHeader: 'Advertencia',
    buttons: ['ACEPTAR']
   });
   await alert.present(); 
  }


}
