import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { ActivatedRoute } from '@angular/router';
import firebase from 'firebase/compat/app'
import { Observable } from 'rxjs';
import { EventService } from 'src/app/servis/event.service';
import { LoginService } from 'src/app/servis/login.service';
import { AlertController } from '@ionic/angular';
@Component({
  selector: 'app-detalles-evento',
  templateUrl: './detalles-evento.page.html',
  styleUrls: ['./detalles-evento.page.scss'],
})
export class DetallesEventoPage implements OnInit {
  datos: any
  nombre: string
  invitados: Observable<any>
  dinero: number
  public listRefEvento: firebase.firestore.CollectionReference;
  constructor(private ruta: ActivatedRoute, public alertController: AlertController, private firestrore: AngularFirestore, private auth: LoginService, private ev: EventService) {
  }
  ngOnInit() {
    const id = this.ruta.snapshot.paramMap.get('id');
    this.getEventDetail(id).then(res => {
      this.datos = res.data()
      console.log(res.data())
    })
    this.listarPartic()
  }
  async getEventDetail(eventId: string): Promise<firebase.firestore.QueryDocumentSnapshot> {
    const user: firebase.User = await this.auth.getUser();
    this.listRefEvento = firebase.firestore().collection(`userProfile/${user.uid}/eventList`);
    return this.listRefEvento.doc(eventId).get();
  }
  async listarPartic() {
    const id = this.ruta.snapshot.paramMap.get('id');
    const user: firebase.User = await this.auth.getUser();
    this.invitados = this.firestrore.collection(`userProfile/${user.uid}/eventList/${id}/eventList`).valueChanges()
    console.log(this.invitados)
  }
  async agregar() {
    if (this.nombre && this.dinero) {
      console.log(this.datos.cost, this.dinero)
      if (this.datos.cost > this.dinero) {
        this.presentAlert2();
      } else if(this.datos.cost < this.dinero) {
       this.presentAlert3();
      }else{
        const id = this.ruta.snapshot.paramMap.get('id');
        this.ev.addGuest(this.nombre, id, this.dinero)
      }

    }else{
      this.presentAlert();
    }
    this.nombre = ''
    this.dinero = null
  }

  async presentAlert() {
    const alert = await this.alertController.create({
    message: 'Por favor ingrese todos los datos',
    subHeader: 'Advertencia',
    buttons: ['ACEPTAR']
   });
   await alert.present(); 
  }

  async presentAlert2() {
    const alert = await this.alertController.create({
    message: 'Saldo insuficiente para realizar el pago',
    subHeader: 'Advertencia',
    buttons: ['ACEPTAR']
   });
   await alert.present(); 
  }

  async presentAlert3() {
    const alert = await this.alertController.create({
    message: 'Su saldo excede el valor del taller. Por favor pague el valor exacto',
    subHeader: 'Advertencia',
    buttons: ['ACEPTAR']
   });
   await alert.present(); 
  }



}

