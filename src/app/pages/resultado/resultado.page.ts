import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router'
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { PacientesService } from 'src/app/pacientes.service';
@Component({
  selector: 'app-resultado',
  templateUrl: './resultado.page.html',
  styleUrls: ['./resultado.page.scss'],
})
export class ResultadoPage implements OnInit {
  respuesta = ''
  conse = ''
  promedio = 0
  resultado: any = 0;
  nombre = "";
  pacientes = []
  constructor(
    private navCtrl: NavController,
    private ruta: ActivatedRoute,
    private firestore: AngularFirestore,
    private servPacien: PacientesService
  ) {
  }

  ngOnInit() {
    this.resultado = this.ruta.snapshot.paramMap.get('resultado');
    this.nombre = this.ruta.snapshot.paramMap.get('nombre');
    this.promedio = parseInt(this.resultado) / 72 * 100
    const dato = parseInt(this.resultado)
    this.servPacien.modificar().subscribe(dat => {
      const buscar = dat.map(p => {
        return {
          dp: p.payload.doc.data(),
          id: p.payload.doc.id
        }
      })
      this.pacientes = buscar
    })
    if (dato <= 24) {
      this.respuesta = 'Sin estrés'
      this.conse = "No existe sintoma alguno de estres.\n" +
        "Tienes buen equilibro, continua asi y contagia a los demas de tus estrategias de afrontamiento!"
    } else if (dato > 24 || dato <= 36) {
      this.respuesta = 'Estrés leve'
      this.conse = "Te encuentras de fase de alarma, trata de identificar el porque a los factores que te causan  estres para poder\n" +
        "ocuparte de ellos de manera preventiva."
    } else if (dato > 36 || dato <= 45) {
      this.respuesta = 'Estrés medio'
      this.conse = "Haz Conciencia de la situacion en la que te encuentras y trata de ubicar que puedes puedes modificar,\n" +
        "ya que si la situacion estresante se prolonga, puedes romper tu equilibrio entre lo laboral  y lo personal.\n" +
        "No agotes tus resitencias!"
    } else if (dato > 45 || dato <= 60) {
      this.respuesta = 'estrés alto'
      this.conse = "Te encuentras en una fase de agotamiento de recursos filosoficos con desgaste fisico y mental.\n" +
        "Esto puede tener consecuencias mas serias para tu salud"
    } else if (dato > 60) {
      this.respuesta = 'estrés grave'
      this.conse = "Busca ayuda"
    }
  }
  descargar() {
    const dato = this.pacientes.filter(dat => dat.dp.cedula == this.nombre)
    console.log(this.resultado, ' ', dato[0].id)
    this.firestore.collection('pacientes').doc(dato[0].id).update({ estres: this.resultado })
  }
}

