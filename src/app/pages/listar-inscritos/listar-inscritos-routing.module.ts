import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListarInscritosPage } from './listar-inscritos.page';

const routes: Routes = [
  {
    path: '',
    component: ListarInscritosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListarInscritosPageRoutingModule {}
