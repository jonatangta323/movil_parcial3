import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-listar-inscritos',
  templateUrl: './listar-inscritos.page.html',
  styleUrls: ['./listar-inscritos.page.scss'],
})
export class ListarInscritosPage implements OnInit {
  pacientes: Observable<any>
  constructor(firestrore: AngularFirestore) { 
    this.pacientes = firestrore.collection('pacientes').valueChanges()
    console.log(firestrore.collection('pacientes').valueChanges())
  }

  ngOnInit() {
  }

}
