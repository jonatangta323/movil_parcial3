import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListarInscritosPageRoutingModule } from './listar-inscritos-routing.module';

import { ListarInscritosPage } from './listar-inscritos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListarInscritosPageRoutingModule
  ],
  declarations: [ListarInscritosPage]
})
export class ListarInscritosPageModule {}
