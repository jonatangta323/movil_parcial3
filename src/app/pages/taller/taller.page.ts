import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { AlertController } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { ModalTallerPage } from '../modal-taller/modal-taller.page'

@Component({
  selector: 'app-taller',
  templateUrl: './taller.page.html',
  styleUrls: ['./taller.page.scss'],
})
export class TallerPage implements OnInit {
  talleres: Observable<any>
  constructor(
    public alertCtrl: AlertController,
    public modalController: ModalController,
    firestrore: AngularFirestore
  ) { 
  this.talleres = firestrore.collection('talleres').valueChanges()
  }

  ngOnInit() {
  }

//  async inscripcion(nombre){
//     let alert = await this.alertCtrl.create({
//       message: '¿Quiere inscribirse en este taller:'+ nombre+ '?',
//       buttons: [
//         {
//           text: 'Cancelar',
//           role: 'cancel',
//           handler: () => {
//             console.log('Cancel clicked');
//           }
//         },
//         {
//           text: 'Aceptar',
//           handler: () => {
//             this.confirmacion();
//           }
//         }
//       ]
//     });
//     await alert.present();
//   }

  async confirmacion(){

    const alert = await this.alertCtrl.create({
      message: 'Su inscripcion en el taller ha sido exitosa',
      subHeader: 'Confirmacion',
      buttons: ['ACEPTAR']
     });
     await alert.present(); 

  }

  async inscripcion(value) {
    const modal = await this.modalController.create({
      component: ModalTallerPage,
      componentProps: {
        'nombre': value.nombre,
        'descripcion': value.descripcion,
        'precio': value.precio
      }
    });
    return await modal.present();
  }


}
