import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListarRiesgoPageRoutingModule } from './listar-riesgo-routing.module';

import { ListarRiesgoPage } from './listar-riesgo.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListarRiesgoPageRoutingModule
  ],
  declarations: [ListarRiesgoPage]
})
export class ListarRiesgoPageModule {}
