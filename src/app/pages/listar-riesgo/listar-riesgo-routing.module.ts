import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListarRiesgoPage } from './listar-riesgo.page';

const routes: Routes = [
  {
    path: '',
    component: ListarRiesgoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListarRiesgoPageRoutingModule {}
