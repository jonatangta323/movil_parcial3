import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-listar-riesgo',
  templateUrl: './listar-riesgo.page.html',
  styleUrls: ['./listar-riesgo.page.scss'],
})
export class ListarRiesgoPage implements OnInit {

   users=[
  {
    cedula:1102345670,
    nombre:'Enrique Jonatan Peña Cantillo',
    indice:'50'
  },
  {
    cedula:1102345670,
    nombre:'Robero Perez Ramos',
    indice:'60'
  },

  ]
  constructor() { }

  ngOnInit() {
  }

}
