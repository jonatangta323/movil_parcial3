import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/servis/login.service';
import { AlertController } from '@ionic/angular';
@Component({
  selector: 'app-registro',
  templateUrl: './registro.page.html',
  styleUrls: ['./registro.page.scss'],
})
export class RegistroPage implements OnInit {

  correo="";
  pass="";

  constructor(
    private auth: LoginService,
    private router: Router,
    public alertController: AlertController
  ) { }

  ngOnInit() {
  }

   registrar(){
  
    if(this.correo!="" && this.pass!=""){
      this.auth.registrar(this.correo, this.pass).then(res => {
        console.log("login", res)
        this.presentAlert2()
        this.router.navigate(['login'])
      })

    }else{
      this.presentAlert();
    }
      
   }

   async presentAlert() {
    const alert = await this.alertController.create({
    message: 'Por favor ingrese su correo y su contraseña',
    subHeader: 'Advertencia',
    buttons: ['ACEPTAR']
   });
   await alert.present(); 
  }

  async presentAlert2() {
    const alert = await this.alertController.create({
    message: 'Usuario registrado con exito',
    subHeader: 'Exito',
    buttons: ['ACEPTAR']
   });
   await alert.present(); 
  }

}
