import { Component, OnInit } from '@angular/core';
import { EventService } from 'src/app/servis/event.service';
import { AlertController } from '@ionic/angular';
@Component({
  selector: 'app-create-event',
  templateUrl: './create-event.page.html',
  styleUrls: ['./create-event.page.scss'],
})
export class CreateEventPage implements OnInit {
  eventName: string;
  eventDate: string;
  eventPrice: number;
  eventCost: number;
  constructor(private serv: EventService,
    public alertController: AlertController) { }

  ngOnInit() {
  }
  createEvent(): void {

    if (!this.eventName || !this.eventDate || !this.eventPrice || !this.eventCost) {
      return;
    }
    this.serv.createEvent(this.eventName, this.eventDate, this.eventPrice, this.eventCost).then(res => {
      console.log(res)
      this.presentAlert();
      this.eventName="";
      this.eventDate="";
      this.eventPrice=null;
      this.eventCost=null;
    })
  }

  async presentAlert() {
    const alert = await this.alertController.create({
    message: 'Evento creado con exito',
    subHeader: 'Advertencia',
    buttons: ['ACEPTAR']
   });
   await alert.present(); 
  }

}
