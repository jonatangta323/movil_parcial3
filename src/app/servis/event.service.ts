import { Injectable } from '@angular/core';
import firebase from 'firebase/compat/app'
import { LoginService } from './login.service';
import { DocumentReference } from "@angular/fire/compat/firestore";

@Injectable({
  providedIn: 'root'
})
export class EventService {

  private event: firebase.firestore.CollectionReference
  constructor(private auth: LoginService) { }

  async createEvent(eventName: string,
    eventDate: string,
    eventPrice: number,
    eventCost: number): Promise<DocumentReference> {
    const user: firebase.User = await this.auth.getUser();

    this.event = firebase.firestore().collection(`userProfile/${user.uid}/eventList`);

    return this.event.add({
      name: eventName,
      date: eventDate,
      price: eventPrice * 1,
      cost: eventCost * 1,
      revenue: eventCost * -1,
      invitados: []
    });
  }

  async getEventList(): Promise<firebase.firestore.QuerySnapshot> {
    const user: firebase.User = await this.auth.getUser();
    if (user) {
      this.event = firebase.firestore().collection(`userProfile/${user.uid}/eventList`);
      return this.event.get();
    }
  }

  async addGuest(nombreInvitado: string, idEvento: string, precioEvento: number) {
    return this.event.doc(idEvento).collection('eventList').add({ nombreInvitado }).then((nuevoInvitado) => {
      return firebase.firestore().runTransaction(transaction => {

        return transaction.get(this.event.doc(idEvento)).then(eventDoc => {
          const nuevoIngreso = eventDoc.data().revenue + precioEvento;
          transaction.update(this.event.doc(idEvento), { revenue: nuevoIngreso });
        });
      });
    });
  }
}
