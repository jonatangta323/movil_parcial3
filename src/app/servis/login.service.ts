import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private auth: AngularFireAuth) { }

  async login(correo: string, pass: string) {
    try {
      return await this.auth.signInWithEmailAndPassword(correo, pass)
    } catch (error) {
      console.log("Error: " + error)
      return error
    }
  }

  async registrar(correo: string, pass: string) {
    try {
      return await this.auth.createUserWithEmailAndPassword(correo, pass)
    } catch (error) {
      console.log("Error: " + error)
      return error
    }
  }
  getUserLogger(){
    return this.auth.authState
  }
  logout(){
    return this.auth.signOut()
  }
  getUser(){
    return this.auth.currentUser
  }
}
