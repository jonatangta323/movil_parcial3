import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../servis/login.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(private auth: LoginService, private router: Router) {}

  logout(){
   this.auth.logout()
   this.router.navigate(['login'])
  }

}
