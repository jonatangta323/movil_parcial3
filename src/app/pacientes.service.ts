import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';

@Injectable({
  providedIn: 'root'
})
export class PacientesService {

  constructor(
   private firestore: AngularFirestore
  ) { }
  modificar(){
     return this.firestore.collection('pacientes').snapshotChanges()
  }
}
